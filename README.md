# assets

Assets for [git.average.name](https://git.average.name/) (logos, customizations, etc). Inspired by [git.gay](https://git.gay/gitgay/assets)

> [!IMPORTANT]
> These templates assume Forgejo `9.0.0`.

Feel free to contribute! See [Forgejo's docs](https://forgejo.org/docs/latest/developer/customization/) and [Gitea's corresponding docs](https://docs.gitea.com/advanced/customizing-gitea) for instructions on how customizations work.

For reference, this repo represents a subset of the contents of the directory at `$FORGEJO_CUSTOM`.

This project's source is licensed under the [BSD Zero Clause License](LICENSE). All contributions to this project's source may be used, copied, modified, and/or distributed for any purpose.
